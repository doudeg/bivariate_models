################################################################################
## file of R environment configuration
################################################################################


###########
## Paths ##
###########

dir.study = getwd()  # exemple: setwd("~/Rstudy_RFA")


if (!exists("dir.functions"))  dir.functions = paste0(dir.study,'/functions')
if (!exists("dir.data"))        dir.data       = paste0(dir.study,"/data")
if (!exists("dir.figures"))    dir.figures   = paste0(dir.study,"/figures")
if (!exists("dir.output"))     dir.output    = paste0(dir.study,"/out")

dir.create(dir.data,showWarnings = F,recursive = T)
dir.create(dir.figures,showWarnings = F,recursive = T)
dir.create(dir.output,showWarnings = F,recursive = T)
#### libraries ####
pckg = c("raster", "maptools", "sp", "rgeos","rgdal","copula","MASS","viridis","goftest","copBasic","stats","mapdata","chron","cartography","in2extRemes","fExtremes","extRemes","ismev","POT","Scale","ggplot2","reshape2","plyr","ggmap","rgdal","grid","gridExtra","lattice","ggpubr","dplyr","texmex","fitdistrplus","evd","igraph","spatgraph","EnvStats","geomorph","ks","kdevine","kdecopula","boot") 

usePackage <- function(p) {
  if (!is.element(p, installed.packages()[,1]))
    install.packages(p, dep = TRUE)
  require(p, character.only = TRUE)
}

lapply(pckg,usePackage)
##### end libraries ####

# source functions directory
# """"""""""""""""""""""""""" 
for (func in list.files(dir.functions)) {
  print(func)
  try(source(paste(dir.functions,func,sep="/")))
}


JointExcureve_file=paste0(getwd(),"/function/Joint_excurves.R")
source (JointExcureve_file)
source(paste0(getwd(),"/function/GenPlot.R"), echo=TRUE)
